# Pattern recognition report

#Introduction 

This report provides an analysis of the customer data set of a telecom company. The knowledge of the probability that a customer will leave a company or not plays a crucial role in the 
field of customer relation management and especially in the customer retention. A company taking this probability into account can where required adapt his services offered to a particular customer. 
We will use data set to refer to the customer data set in the remaining of this report.

We started with a descriptive analysis of the data set. Before performing any statistical analysis, it is important to familiarize with the data at hand in order to know it inside-out. Afterward we 
analyse the data set subsequently using the classifier algorithms logistic regression (simple, regularized and with local search), K-nearest neighbour, Support Vector Machine and Neural Networks. 
Thereby, we focus meanly on the study of the complexity parameters and on the estimated accuracy of the output model selected by each algorithm.




## Descriptive data analysis

The descriptive data analysis helped us to gain insight into the existing variables the data set consists of, the representation of theses variables and the independence relation between them. 
The data set consists of 20 input variables, factors determining if a customer will churn or not and one target variable expressing the churn prediction. The data set originates from 3333 customers 
of the telecom company. 

First, we studied the representation of the data set variables, that is, we explored the distribution of values of each variable. Due to the number of variables the analyse of the variable distribution
was limited to the most interesting variables we picked out by observing the values in the data set. We begin with the distribution of the target values. Our motivation is explained by the fact that the
model output by the statistical analyses are primarly based on the different values of the target variable Churn. If it only contains one single value, any model trained on the data will always predict this value.  
The R histogramm representation of the variable Churn raises some expectation about the possible value of Churn, the statistical algorithms we applied will more likely predict. In fact, Churn consists
of a tiny number of cases leading to the 1, a total of 483 compared to 2850 for 0. Thus, the algorithms are likely to predict the customer not to churned.

hist(data$Churn)    

A further analysis of the data shows a few remarkable results. We quickly see, we can discard the Phone number attribute. The phone number attribute is a surrogate as described in the documentation, 
it stemed from a randow process and is assigned to each person as ID. Because these IDs are unique to each person, they dont not represent any additional value for the model to be trained. A classification based on the phone number 
will definetly not make much sense, therefore the Phone number variable is being dropped out of the input variables.

Furthermore the data set contains two categorical variables State and Area Code. The interesting characteristic for a categorical variable are the frequency of occurence and the range of values.
For the customer state variable there are 51 different possible values and on average there are only 65.4 customers from each state. That seems a quite low number of examples to draw meaningful 
conclusions from. With an variance of 139.27 indicating, that the values of state spread out over a large range, we decided to keep it as input variables. 

length(summary(data$State))
[1] 51
mean(summary(data$State))
[1] 65.35294
var(summary(data$State))
[1] 139.2729

Because State is a categorical variable taking non-numeric values, it needs to be preprocessed. We encoded it using 1-K encoding. That is, we arranged the different values of state into a new 
set of variables, one and only one for each value and replaced the state attribute by the new set. The new set consist of the variables StateAK, StateAl, StateAR, stateAZ, and so on. Doing so,
all of the state variables in the new set are mutually exclusive. One variable at a time can take the value 1 while all others are 0.    	

![](./report_files/figure-html/unnamed-chunk-2-1.png) 
If we plot a histogram of Area Code's we see that there are only 3 codes used. But the codes are represented as integers. That would mean that there is an inherent order to the area codes, 
but we can safely suppose that is not true. We fixed that by treating as categorical variables and again by using 1-k encoding.

Besides the Churn variable and the categorical variables there are also two binary variables Intl.Plan and VMail.Plan in the data set. These can take the value no or yes. Because most classification 
algorithms only support numerical values we reencoded them as 0 and 1 respectively. The representation of the remaining attributes did not show characteristcs that necessitate to be further considered.  

After we explored characteristics of interest in the single variables, we examined the relationship among the input variables. The correlations in inputs data might negatively affect the quality  
of the model resulting from the statistical analyses. There are some attributes that are highly correlated. As the variables number of minutes, number of calls and charge for a given time of day
are all related to a daily activity, we could expect a possible correlation between them. We used the R function cor to find existing dependencies within this set of variables. 


Table: Correlation matrix of Day calling information

              Day.Charge    Day.Mins   Day.Calls
-----------  -----------  ----------  ----------
Day.Charge      1.000000   1.0000000   0.0067530
Day.Mins        1.000000   1.0000000   0.0067504
Day.Calls       0.006753   0.0067504   1.0000000
The correlation coefficient of Day.Mins and Day.Charge can be read out of the output of cor. Observing the correlation coefficients between Day.Charge and Day.Mins, values near one (0.9999) are being output. 
That is an indication of the strong dependence between those variables. Thus, both variables can not be kept in the inputs without affecting the model. Therefore, We can safely remove either the Day.Charge 
or the Day.Mins variables. We decided to remove the Day.Mins from the inputs variables. Another option could have been to hold one of the variable fixed while taking the second unchanged. We prefered the 
first variant over this option since doing so we avoided searching for an estimation of a suitable value of the variable fixed. Regarding to the correlation of Day.Mins and Day.Charge similar dependences can
be observed between Eve.Mins and Eve.Charge, Night.Mins and Night.Charge, Intl.Mins and Intl.Charge. Suprisingly, opposed to our first expectation, the number of calls is uncorrelated with other 2 variables. 
In fact, we obtained for the correlation with Day.Charge and Day.Mins the value 0.0067, that is near zero. 

After reencoding factor variables, adding the new variable set of variables for State and removing some varibles our data set contains 65 predictive variables plus the target variable we trained models with the 
the given algorithms. A function score was implemented to compute accuracy, precision, recall and F-measure achieved from the predictions.  

## Logistic regression

We considered the training of the model with logistic regresssion without interaction terms. We executed logistic regression on our preprocessed data set using the following commands:


Table: Logistic regression on all the data using function glm.

             naive.res
----------  ----------
Accuracy     0.8673867
Precision    0.2629400
Recall       0.5962441
F1           0.3649425
Since we did not apply any test data on the output model the accurancy achieved on the training data is 0.867. Thus, if we simply predict the majority class, a correct classification will be obtained with a 
probability of 86.7%. Unfortunately, in case this naive assumption is not followed, we cannot give any substantial claim to how it will preform on some test samples. The model might extremely fit to the traning set,
as nothing was made to prevent an eventual overfitting. But, there is no way to test that hypothesis without a seperate train set.

Looking at the fitted coefficients, following variables with significant p values capture our attention: Intl.Plan, VMail.Plan, VMail.Message, Intl.Calls, CustServ.Calls and for the state variables set StateCA, StateMT,
StateNJ, StateSC, StateTX. Within these variables some are categorical and others are not. To interpret the coefficients of the model output by the gml in a convenient way we decide to consider separatly categorical and 
non-categorical variables.   

The non-categorical variables with value considerably differing from zero are Intl.plan, CustServ.Calls and VMail.Plan, respectively with the values 2.2, 0.5 adn -2.1. In order to interpret the output values, we pass each 
of them to the function to the R exponential function exp(). The exponental values obtained are for Intl.Plan 8.92, for CustServ.Calls 1.71 and for VMail.Plan 0.12. These values represent the odds of customer churned against
customer not churned. Thus, The positive coefficients in the model mean an increase of this ratio, the negative one a decrease. One additional unit in Int.plan for example leads to an increase of the ratio odds by 8.92 if we 
consider all other variables to be inactive. Obviously, the conclusion can be made that customers will be more likely to churned with increase of the international plan. A similar effect can be recorded for CustServ.Calls. 
Therefore, with one unit of VMail.Plan added we can expect the customer not to churned. 

Besides the non-categorical variables the state set of variables have positive coefficients with values around 1,5. There is hard to give any meaning to this coeficients (maybe the telephone company was agains guns, which caused the Texas people to switch).

### Error measures

As we previously noticed, logistic regression achieved an accuracy of 86.7%. At first glance one might assume, this is a good preformance, however it hardly outpreforms the naive classification algorithm, which always predicts the majority class. 
Such an algorithm would achieve a whooping 85% percent accuracy score. A good way to visualize actual classificator performance is by drawing a confussion matrix. From confussion matrix we see that the logistic regression classifier is very reserved when predicting Churn and in addition it also achieves much lower accuracy when predicting churn.


Table: Confussion matrix.

         0     1
---  -----  ----
0     2764   356
1       86   127

Because accuracy is not the most informative score in this context, we present some other measures that estimate the goodnes of fit. Precision mesures the number of correct positive predictions divided by total number of positive predictions. Recall is another measure often used and measures the number of correct positive predictions divided by the total number of positive cases. Also common in machine learning literature is a measure that incorporates both recall and precision called F1 measure. All 3 measures defined here give a score between 0 and 1.

Using F1 score as a measure of preformance we see that unregulazired logistic regression preformed rather poorly.

$$
precision = TP / (TP + FP) \\
recall = TP / (TP + FN) \\
F1 = 2 * precision * recall / (precision + recall)
$$

Prior to applying the following statistical algorithms to the data set. We split the data into a training set of 1500 rows, a validation set  and test set consisting of 1000 rows each consisting respectively of the total numbers of rows 1500, 8333 and 1000. We use the library LiblineaR to train the model on the data set and tune the cost parameter with cross validation, 
that is, training a model for different values of the cost parameter. Because of the initial analysis of the distribution of the target variable (recall: 85% of the target are 0), if we divide the data set into the 
given proportion for the training, validation and the test (respectively 1500, 833 and 1000) without further ado, we will end up with several chunks of traning set with a single value of the target variable churn. 
In that case, the cross validation would be less efficient. Therefore, before applying cross validation on the combination of training and validation set, we make sure that each set consists of around 85% of the 0s for the variable Churn and 15% of 1s. And we split the data randomly. In the algorithm we use cross validation with 10 folds as it is a common choice. 

## Reguralized logistic regression

In order to train a model using regularized logistic regression we use the r function LibneaR. We pass the combination of the obtained training and validation set to the LiblineaR function along with the number of folds and the estimates of the cost parameter. Here the cost parameter represents the complexity parameter. For the estimates of the cost parameter with 
decide to use the set of value {0, 0.001, 0.005, 0.01, 0.25, 0.05, 0.075, 0.1, 1, 2} to run a cross validation. We try out thoses values and observe the prediction accuracy for each value. At the end we select the cost parameter with the highest accuracy. (See function performLogistic). 

![](./report_files/figure-html/unnamed-chunk-6-1.png) 


Table: LR.

         Accuracy   Precision   Recall   F1
------  ---------  ----------  -------  ---
Train           1           1        1    1
Test            1           1        1    1
For the value 0.05 for the cost parameter we obtain the highest model accuracy of 1. This applies for every value greater than 0.05. Thus, We could choose a value for the cost parameter in the range [0.05, +∞]. However, if the cost parameter is too large the model will get a large bias. That could have the consequence that all the weights of the inputs variables are located around 0. So, We finally fix the cost parameter to 0.05. The prediction based on resulting model gives the following table:  

|      | Accuracy| Precision| Recall| F1|
|:-----|--------:|---------:|------:|--:|
|Train |        1|         1|      1|  1|
|Test  |        1|         1|      1|  1|

Even though we trained and tested on the same data set, we obtain a maximal accuracy for both training and prediction.

## K Nearest Neighbours

We train the KNN models for 20 different values of K. We pick the best model using the missclasification error. To avoid overfiting on the value of K, we use 10-fold cross validation.

![](./report_files/figure-html/unnamed-chunk-8-1.png) 

After using cross validation to find the best parameter k, we can evalute the final model on the test set. For this we use the whole training set, without validation. Obtained results indicate the trained models perforamnce on 1000 examples from the train set.


Table: Scores of the final KNN model (K=7)

              Accuracy   Precision      Recall          F1
----------  ----------  ----------  ----------  ----------
Train set    0.8704419   0.1157270   0.9069767   0.2052632
Test set     0.8592814   0.0479452   0.7777778   0.0903226



We have run KNN algorithm using 10 different values of k. Our results are presented in the table given above. Obtained f1 scores varies wildly between different values of k1, but best obtained result seems to be at k = 4 and k = 8 with predictes f1 scores of 0.98. The highest obtained accuracy however was obtained with k=6, but f1 scores for that parameter is somewhat lower at 0.93. 

We have to take results of the KNN library with a grain of salt. It compares data rows using euclidean distance norm. This means that scaling of columns greatly affects predictions. In our preprocessing steps we have removed the Charge fields and decided to keep Minutes field. Taking a closer look at those fields we see that the value of minutes is aproximately 5 times the value of data. But that means that when using Minutes data, KNN will give much bigger weight to that attribute compared to other than when using charge. The following table shows the same analysis when ignoring Minutes data and using charge instead.



Another think we can try is normalizing the data. This means centering each columns mean at 0 and then scaling it so that standard deviation becomes one. Hopefuly this will remove some problems arising from relative differences in scaling of different attributes. Results are much better, and we can see that in general F1 score increased by about 0.05 points for each K.

![](./report_files/figure-html/unnamed-chunk-10-1.png) 

Again we train the model using the best parameters and present the scores.


Table: Scores of the final KNN model (K=1)

              Accuracy   Precision   Recall          F1
----------  ----------  ----------  -------  ----------
Train set    1.0000000   1.0000000        1   1.0000000
Test set     0.9850299   0.8972603        1   0.9458484

## Local search

Our model currently has about 60 attributes. What we want is to use(select) only those attributes that bring something usefull in predicting the Churn rate. We might also be interested in combinations of attributes that we currently have (if they will sufficiently increase the accuracy of our model).

We can preform the search through the space of models using the stepAIC function. This function takes as an anput a predictor such as GLM or LM and a space of models to try. It preforms a local greedy search. At each step it tries all neighbouring models. Neighboring model is a model obtained by adding or removing a single term. Models are scored with the AIC score. This means that the obtained 'model' fit on the data is penalized by the complexity of the model. 

There are however no gurantes that such a greedy search will find the best model. This can be remedied using multiple random restarts. Another problem that arrises is overfitting of the data. This can be remedied by doing cross validation for each model we are scoring. 

We have used the stepAIC function with a linear model(glm). Results are given in the following table. Best model is also evaluated on the test data, from which we can see the true accuracy of the found model. Obtained scores on both train and test data are very close to each other, which indicates that the model did not overfit.




```r
step.model <- glm(Churn ~ ., family=binomial, data = dataS$tv)
## We search the space of all models with single terms
## We might want do add scope= ~.^2 later...
aic <- stepAIC(step.model, ~., trace=FALSE)

AIC.train.scores <- scores(predict(aic, type="response") > 0.5, dataS$tv$Churn)
AIC.test.scores <- scores(predict(aic, type="response", newdata=dataS$test) > 0.5, dataS$test$Churn)

AIC.res <- rbind(AIC.train.scores, AIC.test.scores )
rownames(AIC.res) <- c("Train set", "Test set")
knitr::kable(data.frame(AIC.res), caption="Best model learned by stepwise search")
```



Table: Best model learned by stepwise search

              Accuracy   Precision      Recall          F1
----------  ----------  ----------  ----------  ----------
Train set    0.8695839   0.2640950   0.6137931   0.3692946
Test set     0.8552894   0.1712329   0.5102041   0.2564103

### SVM

We have decided to train our Support Vector Machine classifier using a RBF kernel. RBF stands for radial basis functions. Radial basis function takes a parameter gamma, that specifies the 'radius' of the kernel. Some other types of kernels are linear, polynomial and sigmoid.

All SVM methods additionaly take a parameter C that represents the cost of missclassification error on the training data.

We tried to find the best value of parameters C and gamma using grid search. The following shows the missclasification errors for different parameters. 

![Parameter search for an SVM classifier.](SVMTraining.png)

After obtaining the best parameters, we trained a full svm on train data and obtained the following scores on the test set.


Table: Scores of the final SVM model (Gamma=0.01, C=16)

              Accuracy   Precision      Recall          F1
----------  ----------  ----------  ----------  ----------
Train set    0.9412269   0.6112760   0.9716981   0.7504554
Test set     0.8712575   0.2534247   0.6491228   0.3645320

### Neural networks

Neural networks are a class of flexible classifiers that preform nonlinear transformations on the data. In general neural networks consist of an input layer, variable number of hidden layers and an output layer. A neural network without a hidden layer is eqvivalent to logistic or linear regression, depending on the activation function in the output layer.

For our tasks we will consider only the class of neural networks with one hidden layer. We will try to determine the best numbers of neurons in this one hidden layer. Another parameter that neural networks use in training is weight decay. Weight decay is eqvivalent to lambda value in logistic regression and is used to prevent overfiting.

In our first attemp to train the neural networks we have used the unnormalized data(data wasnt scaled to zero mean and variance 1). All trained neural networks preformed very poorly in that case and just assigned all values to class 0. After normalizing the data obtained results are better. Another important observation in trainning neural networks is in regard to the local optimums. Neural networks work in a higly nonlinear space. As a consequence any numerical optimization algorithm is bound to get stuck in a local minima. We try to offset this fact by preforming a few random restarts (starting with random initial weights) each time we train a neural network.

Results of hyper-parameter search are presented in the following figure.

![Parameter search for neural networks.](NNtrain.png)


Table: Neural network performance.

              Accuracy   Precision   Recall   F1
-----------  ---------  ----------  -------  ---
Train test           1           1        1    1
Test set             1           1        1    1



### Classificator showdown

We threw several different classification algorithms at the problem of predicting churn rates. In the following table we summarize the preformance of different classificators and compare them to each other.

We see that achieved preformances vary greatly among classificators. First row is unregularized logistic regression that provides a general baseline. All classificators preform better except stepAIC. We try to explain that by the fact that we were not able to include a validation set in model search algorithm of stepAIC.

All other classifiers greatly outpreformed the baseline achieved by logistic regression. For some, and we especialy note KNN here, dataset had to be preprocessed some more by preforming normalization of the data. In fact this is a general recomendation for preforming classification, and some implementations of classification algorithms even do it automaticly.

Best performance, which is a perfect score, was suprisingly achieved by regularized logistic regresion and neural networks. As neural networks are stricly more flexible than logistic regression, their perferct score was expected as a consequence. The only problem could be their overfitting on the data, but we did not notice such problems in our analysis.

Of interest is also the performance of SVM. Although their accuracy score is almost excatly the same as that of our baseline classificator the F1 score is higher. From this we can conclude that SVM tried to find a good balance of classifying both classes instead of focusing just on the majority class. 



Table: Overview of all classificators.

                                    Accuracy   Precision      Recall          F1
--------------------------------  ----------  ----------  ----------  ----------
Logistic regression                0.8673867   0.2629400   0.5962441   0.3649425
Regularized logistic regression    1.0000000   1.0000000   1.0000000   1.0000000
Support vector machine             0.8712575   0.2534247   0.6491228   0.3645320
KNN                                0.8592814   0.0479452   0.7777778   0.0903226
KNN (normalized)                   0.9850299   0.8972603   1.0000000   0.9458484
Stepwise model search              0.8552894   0.1712329   0.5102041   0.2564103
Neural networks                    1.0000000   1.0000000   1.0000000   1.0000000
