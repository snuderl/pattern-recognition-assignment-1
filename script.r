source('util.R')

data <- read.csv('churn.txt', header=TRUE, sep=",")



### Run logistic regression on everything except phone
prog.logreg <- glm(Churn ~ . - Phone, data=data, family=binomial)
summary(prog.logreg)

t <- table(data$Churn, as.numeric(prog.logreg$fitted > 0.5))
acc <- (t[1] + t[4])/sum(t)

### KNN
library(class)

### Remove phone
data = subset(data, select=-Phone)
### Reencode states and area using 1-k
state_enc <- data.frame(model.matrix(~State-1, data))
area_enc <- data.frame(model.matrix(~Area.Code-1, data))
data = subset(data, select=-State)
data = subset(data, select=-Area.Code)
data = cbind(data, state_enc, area_enc)
### Change Intl.Plan and VMail plan to binary
data$Intl.Plan <- as.integer(data$Intl.Plan) - 1
data$VMail.Plan <- as.integer(data$VMail.Plan) - 1
### Split the data into train and test sample.

### Remove Minutes attribute for each time of day. This is supported by the correlation analysis.
data = subset(data, select=-Day.Charge)
data = subset(data, select=-Night.Charge)
data = subset(data, select=-Eve.Charge)
data = subset(data, select=-Intl.Charge)

smp_size <- floor(0.75 * nrow(data))
train_ind <- sample(seq_len(nrow(data)), size = smp_size)
train <- data[train_ind, ]
test <- data[-train_ind, ]

classification_matrix <- table(test$Churn, data.knn)
classification_matrix



s = c()
names = c()
for(k in 1:5){
  data.knn <- knn(train[,-1], test[,-1], train$Churn, k=k)
  s <- c(s, scores(data.knn, test$Churn))
  names = c(names, paste(c("k",k), collapse=""))
}
results <- matrix(s, ncol=4, dimnames=list(names, c("acc", "prec", "recall", "f1")))
res <- data.frame(results)

normalize <- function(x) { 
  x <- sweep(x, 2, apply(x, 2, min)) 
  sweep(x, 2, apply(x, 2, max), "/") 
} 

