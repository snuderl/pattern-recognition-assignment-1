Churn is a term used to indicate a customer leaving the service of one company in favor of another company.
The data set contains 20 variables about 3333 customers of a telecom company along with an indication of whether or not the customer churned.

The variables are:

1. State: categorical, for the 50 states and the District of Columbia
2. Account length: integer-valued, how long account has been active
3. Area code: categorical
4. Phone number: essentially a surrogate for customer ID
5. International Plan: binary, yes or no
6. VoiceMail Plan: binary, yes or no
7. Number of voice mail messages: integer-valued
8. Total day minutes: continuous, minutes customer used service during the day
9. Total day calls: integer-valued
10. Total day charge: continuous
11. Total evening minutes: continuous, minutes customer used service during the evening
12. Total evening calls: integer-valued
13. Total evening charge: continuous
14. Total night minutes: continuous, minutes customer used service during the night
15. Total night calls: integer-valued
16. Total night charge: continuous
17. Total international minutes: continuous, minutes customer used service to make international calls
18. Total international calls: integer-valued
19. Total international charge: continuous
20. Number of calls to customer service: integer-valued
21. Churn: binary, 1 if customer churned, 0 otherwise